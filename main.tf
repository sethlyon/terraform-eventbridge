provider "aws" {
  region = "eu-west-1"

  # Make it faster by skipping stuff
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

locals {
    tags = {
        Feature = "${var.config.feature}"
        environment = "${var.config.environment}"
     }
    longName = "${var.config.environment}-${var.config.feature}-${var.config.eventName}"
}

module "eventbridge" {
  source = "terraform-aws-modules/eventbridge/aws"

  bus_name = var.config.environment

  rules = {
      logs = {
          description = "capture all logs"
          event_pattern = jsonencode({ "account": ["053215648518"]})
          enabled = true
      }
  }

  targets = {
      logs = [
        {
            name = "log-everything-to-cloudwatch"
            arn = aws_cloudwatch_log_group.logGroup.arn
        }
    ]
  }

  tags = "${local.tags}"
}

resource "aws_sqs_queue" "dlq" {
  name                      = "${local.longName}-dlq.fifo"
  max_message_size          = 256000
  message_retention_seconds = 345600
  receive_wait_time_seconds = 20
  fifo_queue                = true
  content_based_deduplication = true

  tags = "${local.tags}"

}

resource "aws_sqs_queue" "fifo" {
  name                      = "${local.longName}.fifo"
  max_message_size          = 256000
  message_retention_seconds = 345600
  receive_wait_time_seconds = 20
  fifo_queue                = true
  content_based_deduplication = true
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.dlq.arn
    maxReceiveCount     = 4
  })

  tags = "${local.tags}"
}

resource "aws_cloudwatch_log_group" "logGroup" {
    name = "/aws/events/${local.longName}"

    tags = "${local.tags}"
}

