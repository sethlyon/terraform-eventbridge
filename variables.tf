
variable "config" {
    type = map
    default = {
        environment = "slyonTestEnv"
        feature = "testFeature"
        eventName = "testEvent"
    }
}
